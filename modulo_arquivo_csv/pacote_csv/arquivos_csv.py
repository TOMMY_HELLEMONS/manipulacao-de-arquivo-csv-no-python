import csv

def extrair_adicionar(nome_arquivo, nome_arquivo_copia, adicionar_linha=False):
    try:
        # fazer uma copia do arquivo csv
        with open(nome_arquivo, 'r') as entrada:
            ler = csv.reader(entrada)
            for linha in ler:
                # Adicionar o conteúdo do arquivo de origem ao novo arquivo
                with open(nome_arquivo_copia, 'a', newline='') as saida:
                    escrever = csv.writer(saida)
                    escrever.writerow(linha)
                saida.close()
        entrada.close()

        if adicionar_linha != False:
            # Adicionar uma nova linha
            with open(nome_arquivo_copia, 'a+', newline='') as saida:
                escrever = csv.writer(saida)
                # EXEMPLO -> ['Lucas', 'Pereira', '0000', '5555', '000', 'ssss']
                escrever.writerow(adicionar_linha)
            saida.close()
    except:
        print("ERRO")


# teste da função
if __name__ == '__main__':
    # exemplo de como usar a função
    extrair_adicionar("arquivos/novo_arquivo_final.csv", "arquivos/copia_2021.csv", ['Lucas', 'Pereira', '0000', '5555', '000', 'ssss'])
                    #  caminho_nome_extensão            caminho_nomeCopia_extensão      Adicionar linha em formato de list
